const mysql = require('mysql')

const pool = mysql.createPool({
    user: 'root',
    password: 'manager',
    database: 'cardb',
    host: 'localhost',
    port: 3306,
    connectionLimit: 20,
})

module.exports = {
    pool,
}