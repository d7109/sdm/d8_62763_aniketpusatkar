const express = require('express')
const db = require('../db')
const router = express.Router()
const utils = require('../utils')

router.get('/', (request, response) => {
    const statement = `
        select * from car ;

    `
    db.pool.query(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/add', (request, response) => {
    const { car_name, company_name, car_prize } = request.body

    const statement = `
        insert into car 
            (car_name, company_name, car_prize)
        values
            (?,?,?)   
    `
    db.pool.query(
        statement,
        [book_title, publisher_name, author_name],
        (error, result) => {
            response.send(utils.createResult(error, result))
        }
    )
})

router.put('/update/:car_name', (request, response) => {
    const { car_name } = request.params
    const { company_name, car_prize } = request.body

    const statement = `
        update car 
            set
            company_name=? , car_prize=?
            where
            car_name = ?
    `
    db.pool.query(
        statement,
        [company_name, car_prize, car_name],
        (error, result) => {
            response.send(utils.createResult(error, result))
        }
    )
})

router.delete('/remove/:car_name', (request, response) => {
    const { book_title } = request.params
    const statement = `
        delete from Book 
            where
            car_name = ?
    `
    db.pool.query(statement, [book_title], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
module.exports = router